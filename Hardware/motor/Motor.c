#include "motor.h"

int PWM_MAX=7200,PWM_MIN=-7200;
int motor1,motor2;  				   //电机装载数据

/*电机初始化函数*/
void Motor_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
	
	GPIO_InitStruct.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_InitStruct.GPIO_Pin=GPIO_Pin_12|GPIO_Pin_13|GPIO_Pin_14|GPIO_Pin_15;
	GPIO_InitStruct.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOB,&GPIO_InitStruct);
}

//刹车
void brake(void)
{
	GPIO_SetBits(GPIOB,GPIO_Pin_12);
	GPIO_SetBits(GPIOB,GPIO_Pin_13);
	GPIO_SetBits(GPIOB,GPIO_Pin_14);
	GPIO_SetBits(GPIOB,GPIO_Pin_15);
}

/*限幅函数*/
void Limit(int *motorA,int *motorB)
{
	if(*motorA>PWM_MAX)
	{
		*motorA=PWM_MAX;
	}
	if(*motorA<PWM_MIN)
	{
		*motorA=PWM_MIN;
	}
	
	if(*motorB>PWM_MAX)
	{
		*motorB=PWM_MAX;
	}
	if(*motorB<PWM_MIN)
	{
		*motorB=PWM_MIN;
	}
}

/*绝对值函数*/
int MyAbs(int p)
{
	return (p>0?p:(-p));
}

/**********************
赋值函数
入口参数:PID运算后的最终PWM值
***********************/
void load(int motor1,int motor2)
{
	//研究正负号，对应正反转
	if(motor1>0)
	{
		Ain1=1;
		Ain2=0;
	}
	else
	{
		Ain1=0;
		Ain2=1;
	}
	TIM_SetCompare1(TIM1,MyAbs(motor1));	//输出电机速度

	if(motor2>0)
	{
		Bin1=1;
		Bin2=0;
	}
	else
	{
		Bin1=0;
		Bin2=1;
	}
	TIM_SetCompare4(TIM1,MyAbs(motor2));
}

