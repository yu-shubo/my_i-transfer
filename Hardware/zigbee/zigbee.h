#ifndef __ZIGBEE_H
#define __ZIGBEE_H

#include "stm32f10x.h"
#include "sys.h"
#include "stdio.h"
#include "stdarg.h"


extern char recv_array[4];		

void zigbee_uart_init(u32 bound);	//���ڳ�ʼ��
void zigbee_sendbyte(USART_TypeDef* pUSARTx,unsigned char data);
void zigbee_send_array(USART_TypeDef* pUSARTx,unsigned char *array,int lenth);
void Zigbee_SendNumber(uint16_t Number,uint8_t Length);
uint32_t Pow(uint32_t x,uint32_t y);
void zigbee_vofa(USART_TypeDef* pUSARTx,float *data,int lenth);
int fputc(int ch, FILE *f);
int GetKey (void);

#endif //__ZIGBEE_H


