#include "control.h"
#include "OLED.h"

/**
 * @brief 开环直行
 * @param 直行距离；直行速度
 * @retval 无
 */
void open_loop(int distant, int pwm)
{
	pathLenth = 0;
	int motor_left, motor_right;
	while (Distant_Limit(pathLenth, &pwm, distant) == 0)
	{
		motor_left = pwm;
		motor_right = pwm;
		load(motor_left, motor_right);
	}
}

/**
 * @brief 循迹PID
 * @param 当前车身角度；目标车身角度
 * @retval PWM
 */

int Line_PWM(int rho, int Taget_rho)
{
	static float rho_Kp = 5, rho_Kd = 0;
	static float PWM_out, error;
	static float last_error = 0;

	error = Taget_rho - rho;

	PWM_out = rho_Kp * error + rho_Kd * (error - last_error);

	last_error = error;

	return PWM_out;
}

/**
 * @brief 距离限制函数
 * @param 当前位置；当前pwm值；预期距离
 * @retval 1 or 0 (1代表超出距离，0代表未达到距离)
 */
int Distant_Limit(int Now_Disdant, int *pwm, int Taget_Distant)
{
	if (Now_Disdant > Taget_Distant)
	{
		brake();  //马上停车
		*pwm = 0; //将PWM置零
		return 1;
	}
	return 0;
}

/**
 * @brief 距离环（使用循迹）
 * @param 目标行进距离；前进速度；行进模式(前进or后退)
 * @retval 无
 */
void Distant_GO(int Taget_Distant, int pwm, int mode)
{
	float line_pwm;
	int motor_left, motor_right;

	pathLenth = 0; //将计数值清零

	if (mode == Mode_Forward) //前进模式
	{
		while (Distant_Limit(pathLenth, &pwm, Taget_Distant) == 0) //在设定范围内
		{
			line_pwm = Line_PWM(rho, Tager_rho);
			motor_left = pwm + line_pwm;
			motor_right = pwm - line_pwm;

			load(motor_left, motor_right); //++
		}
	}
	else if (mode == Mode_Back) //后退模式
	{
		while (Distant_Limit(pathLenth, &pwm, Taget_Distant) == 0) //在设定范围内
		{
			line_pwm = 0.3 * Line_PWM(rho, Tager_rho);
			motor_left = pwm + line_pwm;
			motor_right = pwm - line_pwm;

			load(-(motor_left + 300), -motor_right); //--
		}
	}
}

/**
 * @brief 转向
 * @param 脉冲值；方向(左1 右2）
 * @retval 无
 */
void Car_turn(int pluse, int mode)
{
	pathLenth = 0;

	while (1)
	{
		if (mode == 1) //向左
		{
			load(-pluse, pluse);
		}
		else if (mode == 2) //向右
		{
			load(pluse, -pluse);
		}
		if (pathLenth > pluse) //达到预设角度
		{
			break;
		}
	}
	load(0, 0); //立马定住
	brake();
}
