#include "timer.h"
#include "encoder.h"

void  TIM1_Init(u16 arr,u16 psc) 
{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;  
    NVIC_InitTypeDef NVIC_InitStructure;  
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE); //时钟使能  

    TIM_TimeBaseStructure.TIM_Period = arr; //设置自动重装载寄存器周期值  
    TIM_TimeBaseStructure.TIM_Prescaler =(psc-1);//设置预分频值  
    TIM_TimeBaseStructure.TIM_ClockDivision = 0; //设置时钟分割  
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;//向上计数模式  
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 1;//重复计数设置  
    TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure); //参数初始化  
    TIM_ClearFlag(TIM1, TIM_FLAG_Update);//清中断标志位  
    TIM_ITConfig(      //使能或者失能指定的TIM中断  
    TIM1,            //TIM1  
    TIM_IT_Update  | //TIM 更新中断源  
    TIM_IT_Trigger,  //TIM 触发中断源                                 //开启定时器1
		  ENABLE           //使能  
    );  
	  //设置优先级  

    NVIC_InitStructure.NVIC_IRQChannel = TIM1_UP_IRQn;    
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;//先占优先级0级  
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;       //从优先级0级  
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;  
    NVIC_Init(&NVIC_InitStructure);   
    TIM_Cmd(TIM1, DISABLE);  //使能TIMx外设 
}

//绝对值函数
int abs(int arg)
{
	return arg>0?arg:(-1*arg);
}


unsigned int pathLenth = 0;

void TIM1_UP_IRQHandler(void)   //控制函数
{
	if  (TIM_GetITStatus(TIM1, TIM_IT_Update) != RESET) //检查 TIM1更新中断发生与否
	{
		pathLenth += abs(Read_Speed(2))+abs(Read_Speed(3));//注意这里是累加
		
		TIM_ClearITPendingBit(TIM1, TIM_IT_Update ); //清除 TIM1 更新中断标志
	}
}


