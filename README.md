# My_iTransfer

#### 介绍
20年全国大学生电子设计大赛F题
<img src = "images/IMG_20230706_083809.jpg">
#### 项目视频 [【21年电赛F题送药小车】stm32标准库+PID算法控制+openmv寻迹+k210神经网络数字识别](https://www.bilibili.com/video/BV1BF411R7s8/?spm_id_from=333.337.search-card.all.click&vd_source=5b7925e1bf71ff5241b1e62ab131eb2d)
#### 使用说明

1.  `Hardware`里面是板载上面的所有外设驱动文件
2.  `openmv`里面是使用openmv的线性回归进行循迹的代码
3.  `k210`里面有训练模型和运行文件，进行对数字的识别

## 代码中可能存在一些问题，需要根据自己的硬件结构和实际情况来，欢迎大家相互学习指正！整理不易，记得star哦 :stuck_out_tongue_winking_eye: 


